// SPDX-License-Identifier: (GPL-2.0+ OR MIT)
/*
 * Copyright (c) 2017 Fuzhou Rockchip Electronics Co., Ltd.
 */

/dts-v1/;

#include "rk3288-tinker.dtsi"
#include <dt-bindings/clock/rockchip,rk808.h>

/ {
	model = "Rockchip RK3288 Asus Tinker Board";
	compatible = "asus,rk3288-tinker", "rockchip,rk3288";

	/* This is essential to get SDIO devices working.
	   The Wifi depends on SDIO ! */
	sdio_pwrseq: sdio-pwrseq {
		compatible = "mmc-pwrseq-simple";
		clocks = <&rk808 RK808_CLKOUT1>;
		clock-names = "ext_clock";
		pinctrl-names = "default";
		pinctrl-0 = <&chip_enable_h>, <&wifi_enable_h>;

		/*
		 * On the module itself this is one of these (depending
		 * on the actual card populated):
		 * - SDIO_RESET_L_WL_REG_ON
		 * - PDN (power down when low)
		 */
		reset-gpios = <&gpio4 28 GPIO_ACTIVE_LOW>, <&gpio4 27 GPIO_ACTIVE_LOW>;
	};

	wireless-wlan {
		compatible = "wlan-platdata";
		rockchip,grf = <&grf>;
		sdio_vref = <1800>;
		status = "okay";
		wifi_chip_type = "8723bs";
		WIFI,host_wake_irq = <&gpio4 30 GPIO_ACTIVE_HIGH>;
	};
};

/*
&gpio0 {
	ts_pins: ts_pins {
		rockchip,pins = <0 RK_PC1 RK_FUNC_GPIO &pcfg_pull_none>;
	 };
};
*/

// https://elixir.bootlin.com/linux/v5.4.54/source/Documentation/devicetree/bindings/input/touchscreen/touchscreen.txt

&i2c1 {
	displayTonny: edt-ft5x06@38 {
		compatible = "edt,edt-ft5406","edt,edt-ft5x06";
		status = "okay";
		reg = <0x38>;
                pinctrl-names = "default";
//                pinctrl-0 = <&ts_pins>;
		interrupt-parent = <&gpio0>;
		interrupts = <17 0x8>; /* HIGH TO LOW INT */
	//	touchscreen-inverted-x;
//		touchscreen-swapped-x-y;
		touchscreen-min-x = <0>;
		touchscreen-min-y = <0>;
		touchscreen-size-x = <1024>;
		touchscreen-size-y = <600>;

	// touch in length
//		touchscreen-x-mm = <1024>;
//		touchscreen-y-mm = <600>;
	};
};

&i2c2 {
	afc0:af-controller@0 {
		status = "okay";
		compatible = "silicon touch,vm149C-v4l2-i2c-subdev";
		reg = <0x0 0x0c>;
	};

	eeprom:m24c08@50 {
		compatible = "at,24c08";
		reg = <0x50>;
	};
};

&i2c3 {
	status = "okay";
};

&i2c4 {
	status = "okay";
};

&io_domains {
	wifi-supply = <&vcc_18>;
};

&pinctrl {
	sdio-pwrseq {
		wifi_enable_h: wifienable-h {
			rockchip,pins = <4 28 RK_FUNC_GPIO &pcfg_pull_none>;
		};

		chip_enable_h: chip-enable-h {
			rockchip,pins = <4 27 RK_FUNC_GPIO &pcfg_pull_none>;
		};
	};
};

&sdio0 {
	bus-width = <4>;
	cap-sd-highspeed;
	cap-sdio-irq;
	clock-frequency = <50000000>;
	clock-freq-min-max = <200000 50000000>;
	disable-wp;
	keep-power-in-suspend;
	mmc-pwrseq = <&sdio_pwrseq>;
	non-removable;
	num-slots = <1>;
	pinctrl-names = "default";
	pinctrl-0 = <&sdio0_bus4 &sdio0_cmd &sdio0_clk>;
	sd-uhs-sdr104;
	status = "okay";
	supports-sdio;
};

&spi2 {
	max-freq = <50000000>;
	status = "okay";

	spidev@0 {
		compatible = "rockchip,spi_tinker";
		reg = <0x0 0>;
		spi-max-frequency = <50000000>;
		spi-cpha = <1>;
	};

	spidev@1 {
		compatible = "rockchip,spi_tinker";
		reg = <0x1>;
		spi-max-frequency = <50000000>;
		spi-cpha = <1>;
	};
};

&uart0 {
	pinctrl-names = "default";
	pinctrl-0 = <&uart0_xfer>, <&uart0_cts>, <&uart0_rts>;
};

