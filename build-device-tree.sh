kernel=$(uname -r)
cp rk3288-tinker-interlaken.dts /usr/src/linux-source-$kernel/arch/arm/boot/dts

#grep -qxF 'rk3288-tinker-interlaken.dtb \' /usr/src/linux-source-$kernel/arch/arm/boot/dts/Makefile || echo 'rk3288-tinker-interlaken.dtb \' >> /usr/src/linux-source-$kernel/arch/arm/boot/dts/Makefile

cd /usr/src/linux-source-$kernel
make rk3288-tinker-interlaken.dtb
cp /usr/src/linux-source-$kernel/arch/arm/boot/dts/rk3288-tinker-interlaken.dtb /boot/dtb/rk3288-tinker.dtb
